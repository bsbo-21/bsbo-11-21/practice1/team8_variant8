```plantuml
@startuml

skin rose

title Classes - Class Diagram


class Agregator {
  +Advertisement[] Advertisement_list
  -Filter()
  +Search()
  +offer()
  +Add_Announcement()
  -Checking_Answers()
  +Online_Application()
}

class Advertisement{
  +string Name
  +int price
  +string description
  +real_estate real_estate
  +real_estate()
  +Create_Announcement()
  +landlord()
}

class real_estate{
 +String Type
 +Int square_footage
}


class offer{
  +Advertisement Advertisement
  +real_estate real_estate
  +int number
  -Renter()
  +Online_Application()
}


real_estate <-> Advertisement 
Advertisement -> Agregator
Advertisement -> offer
offer <-> Agregator
@enduml
```
